<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.baidu.ueditor.ActionEnter,com.je.core.util.*,net.sf.json.JSONObject"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%

    request.setCharacterEncoding( "utf-8" );
	response.setHeader("Content-Type" , "text/html");
	
	String rootPath = application.getRealPath( "/" );
	String result = new ActionEnter( request, rootPath ).exec();
	//处理图片的url
	String action = request.getParameter("action");
	String imageUrlType = request.getParameter("imageUrlType");
	if("uploadimage".equals(action) && "base64".equals(imageUrlType)){
		JSONObject jb = JSONObject.fromObject(result);
		//System.out.println(result);
		if("SUCCESS".equals(jb.get("state").toString())){
			String base64 = FileOperate.encodeBase64File(rootPath+jb.get("url").toString());
			jb.put("url","data:image/png;base64,"+base64);
		}
		result = jb.toString();
	}
	out.write( result );
	
%>