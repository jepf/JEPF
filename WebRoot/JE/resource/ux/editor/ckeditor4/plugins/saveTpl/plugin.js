﻿ //自定义功能：选择模版
 CKEDITOR.plugins.add('saveTpl', {
     init: function (editor) {
     	var a = {
	         exec: function (editor) {
	         	editor.ownerCt.saveTableTpl(editor);
	         }
	     };
         editor.addCommand('saveTpl', a);
         editor.ui.addButton('saveTpl', {
             label: '保存模版',
             icon: this.path + 'save.png',
             command: 'saveTpl'
         });
     }
 });
