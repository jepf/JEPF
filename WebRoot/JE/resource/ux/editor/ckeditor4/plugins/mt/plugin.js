﻿ //自定义功能：选择模版
 CKEDITOR.plugins.add('mt', {
     init: function (editor) {
     	var a = {
	         exec: function (editor) {
	         	editor.ownerCt.insertTableTpl(editor);
	         }
	     };
         editor.addCommand('mt', a);
         editor.ui.addButton('mt', {
             label: '添加模版',
             icon: this.path + 'mt.png',
             command: 'mt'
         });
     }
 });
