/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//回车输入的标签
    config.enterMode = CKEDITOR.ENTER_BR;
    
	config.extraPlugins = 'mt,previewtable,saveTpl';
	config.toolbar_JEtbar =
	[
	    [ 'NewPage','DocProps','Preview','Print','-','Templates' ],
	    [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
	    [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ],
	    [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ],
	    '/',
	    [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
	    [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ],
	    [ 'Link','Unlink','Anchor' ],
	    [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak' ],
	    '/',
	    [ 'Styles','Format','Font','FontSize' ],
	    [ 'TextColor','BGColor' ],
	    [ 'Maximize', 'ShowBlocks','-','Source' ]
	];
	config.toolbar_JETabletbar =
	[
	    [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','RemoveFormat' ],
	    [ 'FontSize','TextColor','BGColor'],
	    [ 'Table','Button','mt'],
	    [ 'previewtable','saveTpl' ],
	    [ 'Source' ]
	];
	config.toolbar_JEReadOnlytbar =
	[
	    [ 'Copy','Preview', '-','Source' ]
	];
};
