<!DOCTYPE HTML>
<%@ page language="java" import="java.util.*"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
String extjs = "/JE/resource/ExtJs";
String mxgraph = "/JE/resource/mxgraph/src";
String version=com.je.core.util.WebUtils.sysVar.get("JE_ICON_VERSION");
if(com.je.core.util.StringUtil.isNotEmpty(version)){
	version=("?version="+version);
}else{
	version="";
}
%>
<html>
  <head>
    <base href="<%=basePath%>">
    <title><%=com.je.core.util.WebUtils.sysVar.get("JE_SYS_TITLE")%></title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">  
	<meta http-equiv="X-UA-Compatible" content="IE=10">  
	<link rel="shortcut icon" href="./favicon.ico"/>
	<!-- ExtJs -->
	<link rel="stylesheet" href="<%=extjs%>/resources/css/ext-all.css"/>
	<script type="text/javascript" src="<%=extjs%>/ext-all.js"></script>
	<!-- ExtJs end -->
	<!-- jQuery 插件 -->
	<script type="text/javascript" src="/JE/resource/jquery/jquery-1.8.3.min.js"></script>
	<!-- jQuery 插件  end -->
	<script type="text/javascript">
			var ExtJsPath = '<%=extjs%>';//js源文件路径
			var basePath = '<%=basePath%>';
			//画布参数
			var mxResourcePath = '<%=mxgraph %>';//画布项目根目录
			var mxBasePath = mxResourcePath;//画布项目资源目录
			var STENCIL_PATH = mxResourcePath+'/stencils';//数据模版资源路径
				//多语言
			var _JE_LOCAL_LANG = Ext.value(Ext.util.Cookies.get('je-local-lang'),'zh_CN');
			if(_JE_LOCAL_LANG == 'en' ){
				//<![CDATA[
			    document.write('<script type="text/javascript" src="/JE/coreApp/locale/je-lang-en.js"><\/script>');
			    //]]>
			}else{
				//<![CDATA[
			    document.write('<script type="text/javascript" src="/JE/coreApp/locale/ext-lang-zh_CN.js"><\/script>');
			    document.write('<script type="text/javascript" src="/JE/coreApp/locale/je-lang-zh_CN.js"><\/script>');
			    //]]>
			}
	</script>
	
	
	<!-- dwr -->
	<script type="text/javascript" src="/dwr/interface/dwrManager.js"></script>
	<script type="text/javascript" src="/dwr/interface/jmsManager.js"></script>
	<script type="text/javascript" src="/dwr/engine.js"></script>
	<script type="text/javascript" src="/dwr/util.js"></script>
	<!-- dwr end-->
	
	<!-- 运行模式 -->
	<link rel="stylesheet" href="/JE/coreApp/lib/JE-common-min.css"/>
	<link rel="stylesheet" href="/JE/resource/css/saas.css"/>
	<script type="text/javascript" src="/JE/coreApp/lib/JE-all-min.js" id="JE-all-min-JS"></script>
	<!-- 平台图标 -->
	<link rel="stylesheet" href="/JE/resource/css/icon.css<%=version%>"/>
	<!-- 用户自定义css/js -->
	<link rel="stylesheet" href="/JE/resource/css/custom.css"/>
	<script type="text/javascript" src="/JE/resource/js/SingletonUtil.js"></script>
	<!-- 用户自定义css/js end-->
  </head>
  <body onfocus="desktopNotifFlag = false;" onblur="desktopNotifFlag = true;" style="margin:0px;">
  		<div id="loadingText" style="background-color: #34495e;width:100%;height:100%;display:table;">
  			<span style="display: table-cell;vertical-align: middle;text-align: center;color:#fff;font-size: 30px;font-family: tahoma,arial,verdana,宋体,sans-serif;font-weight: bold;">
  				Loading......
  			</span>
  		</div>
  </body>
</html>








