<!-- LOGIN -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html; charset=UTF-8" import="com.je.core.constants.LoginErrorType"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="shortcut icon" href="./favicon.ico"/>
<link rel="stylesheet" href="/JE/resource/chosen/chosen.css"/>
<!-- 缓存 -->
<link rel="stylesheet" href="/JE/resource/ExtJs/resources/css/ext-all.css"/>
<link rel="stylesheet" href="/JE/resource/css/comm.css"/>
<link rel="stylesheet" href="/JE/resource/css/icon.css"/>
<script type="text/javascript" src="/JE/resource/ExtJs/ext-all.js"></script>
<!-- 缓存 -end-->
<script type="text/javascript" src="/JE/resource/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/JE/resource/chosen/chosen.jquery.min.js"></script>
<title><%=com.je.core.util.WebUtils.sysVar.get("JE_SYS_TITLE")%></title>
<link href="/JE/resource/css/login.css" rel="stylesheet" type="text/css" />
</head>
<body>
<%
	// 读取cookie
	String loginUserCode = "";
	javax.servlet.http.Cookie userCode = org.springframework.web.util.WebUtils.getCookie(request, "loginUserCode");
	if(null != userCode) {
		loginUserCode = userCode.getValue();
	}
	String version=com.je.core.util.WebUtils.sysVar.get("JE_ICON_VERSION");
	if(com.je.core.util.StringUtil.isNotEmpty(version)){
		version=("?version="+version);
	}else{
		version="";
	}
	//处理登录错误消息  
	Exception e=(Exception)session.getAttribute("SPRING_SECURITY_LAST_EXCEPTION");
	String error="登录密码错误!";
	if(e!=null){
		String errorType=e.getMessage();
		if(e instanceof org.springframework.security.concurrent.ConcurrentLoginException){
			error="该用户已经在别处登录!";	
		}else if(LoginErrorType.NONE.equals(errorType)){
			error="该用户不存在!";
		}else if(LoginErrorType.DISABLED.equals(errorType)){
			error="该用户已被禁用!";
		}else if(LoginErrorType.NOSYS.equals(errorType)){
			error="该用户不是系统用户!";
		}else if(LoginErrorType.INVALID.equals(errorType)){
			error="该用户已经失效!";
		}else if(LoginErrorType.ERRORPROXY.equals(errorType)){
			error="代理登录失败!";
		}else{
			error="登录密码错误!";
		}
	}
	if(null== request.getParameter("error")){
		error="";
	}
%>
<div class="wrap">
	<div class="header">
    	<h1>JE快速开发平台</h1>
    </div>
    <div class="main">
    	<form name="userform" method="post" action="/j_spring_security_check">
        <div class="hidden-input">
        <input type="hidden" name="" />
        <input type="hidden" name="" />
        <input type="hidden" name="" />
        </div>
        <div class="user-name">
            <label class="lb">用户名：</label>
            <input name="j_username" type="text" id="j_username" value="<%=loginUserCode%>" class="ipt-t" onfocus="this.className+=' ipt-t-focus'" onblur="this.className='ipt-t'" />
        </div>
        <div class="pass-word">
            <label class="lb">密&nbsp;&nbsp;&nbsp;&nbsp;码：</label>
            <input name="j_password" type="password" id="j_password" class="ipt-t" onfocus="this.className+=' ipt-t-focus'" onblur="this.className='ipt-t'" />
        </div>
       <div id="dept-select" class="dept-select">
        	<label class="lb">部&nbsp;&nbsp;&nbsp;&nbsp;门：</label>
        	<select id="j_dept" class="dept_select chzn-select" name="j_dept">
        		<option class=dept value=default>默认</option>
        	</select>
        </div>
       <div id="lang-radio"  class="dept-select" style="color:#fff;padding:0px 0px 0px 40px;">
       		<div style="display:table;float:left;">
       			<img src="/JE/resource/css/img/login/china.png" style="margin-right:5px;">
       			<span id="j_lang_zh" style="cursor: pointer;display: table-cell;vertical-align: middle;">简体中文</span>
       		</div> 
       		<div style="display:table;float:left;margin-left:30px;">
       			<img src="/JE/resource/css/img/login/english.png"style="margin-right:5px;">
       			<span id="j_lang_en" style="cursor: pointer;display: table-cell;vertical-align: middle;">English</span>
       		</div> 
        </div>
        <!-- <div id="sentry-select" class="sentry-select">
        	<label class="lb">岗 位：</label>
        	<select id="j_sentrys" class="role_select chzn-select" name="j_sentrys">
        	</select>
        </div>
    	 <div class="login-time">
        <label class="lb">登录日期：</label>
            <input name="" type="text" class="ipt-t" value='2014-04-13' onfocus="this.className+=' ipt-t-focus'" onblur="this.className='ipt-t'" />
        </div>-->
        <div id="login-button" class="login-button" style="margin-top:-114px;">
        	<input name="" type="submit" value="登录" class="button b-left" id="btnSearch" onmouseover="this.className+=' button-focus'" onmouseout="this.className='button b-left'" />
            <input name="" type="reset" value="重置" class="button" onmouseover="this.className+=' button-focus'" onmouseout="this.className='button'" />
        </div> 
        <div id="msg" color=red><%=error%></div>       
        </form>
    </div>
    <div class="footer">
    <p><%=com.je.core.util.WebUtils.sysVar.get("JE_SYS_COPYRIGHT")%></p>
    </div>
    
    <!-- 缓存图片 -->
    <div style="display:none;" id="imgDiv">
		<img src="/JE/resource/css/img/form/form-more-trigger.png"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/refresh.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tree/leaf.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/loading.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/checkbox.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/group-collapse.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/page-next.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/date-trigger.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/radio.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/spinner.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/page-last.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/refresh-disabled.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/page-last-disabled.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/page-next-disabled.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/page-prev-disabled.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/page-first-disabled.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/button/arrow.gif"/>
		<img src="/JE/resource/css/img/form/form-tree-trigger.png"/>
		<img src="/JE/resource/css/img/form/search.png"/>
		<img src="/JE/resource/css/img/checkbox.png"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/search-trigger.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/clear-trigger.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tab/tab-default-close.gif"/>
		<img src="/JE/resource/css/img/new.gif"/>
		<img src="/JE/data/upload/icon/733e7329-1115-4528-ab34-78a614cb3ecc.png"/>
		<img src="/JE/data/upload/icon/df96e595-89ec-475e-9c3d-bdd82aa53864.png"/>
		<img src="/JE/resource/ux/SkitterSlideshow/images/ajax-loader.gif"/>
		<img src="/JE/resource/ux/SkitterSlideshow/images/sprite-clean.png"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tree/folder.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tree/arrows.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/util/splitter/mini-left.gif"/>
		<img src="/JE/resource/css/icon.png<%=version%>"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/trigger.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/form/exclamation.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tree/s.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tab-bar/default-scroll-left-top.gif"/>
		<img src="/JE/resource/css/img/header.png"/>
		<img src="/JE/resource/css/img/form/form-color-trigger.png"/>
		<img src="/JE/resource/css/img/form/form-file-trigger.png"/>
		<img src="/JE/resource/ExtJs/resources/css/images/grid/grid3-hd-btn.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tools/tool-sprites.gif"/>
		<img src="/JE/resource/ExtJs/resources/css/images/tab-bar/default-scroll-right-top.gif"/>
		
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	//中英切换
	$("#j_lang_zh").click(function(){
		$("#j_lang_zh").css('fontWeight','bold');
		$("#j_lang_en").css('fontWeight','normal');
		$("#j_lang_zh").css('color','#fff');
		$("#j_lang_en").css('color','#cecece');
		Ext.util.Cookies.set('je-local-lang','zh_CN');
	});
	$("#j_lang_zh").click();
	$("#j_lang_en").click(function(){
		$("#j_lang_en").css('fontWeight','bold');
		$("#j_lang_zh").css('fontWeight','normal');
		$("#j_lang_en").css('color','#fff');
		$("#j_lang_zh").css('color','#cecece');
		Ext.util.Cookies.set('je-local-lang','en');
	});
	
	//如果取消语言选择，放开下面注释
	$("#lang-radio").hide();
	$("#login-button").css("marginTop","-90px");
	
	
	$("#j_username").focus();
	//渲染下拉框
	$("#j_dept").chosen({disable_search: true});
	//$("#j_lang").chosen({disable_search: true});
	//用户名失去焦点时触发事件，根据登录名修改角色下拉选项
	$("#j_username").blur(function(event){
		var userCode = $('#j_username').val();
		$.ajax({
			url:'/je/loginAction!getIdentityByUserCode.action',
			data:{userCode:userCode},
			async:false,
			success:function(data){
					data = eval('('+data+')');
					$('#j_dept option.dept').remove();
					if(data.success){
						if(data.obj.DEPTINFO && data.obj.DEPTINFO!=""){
							$.each(data.obj.DEPTINFO,function(i,obj){
								if(obj.DEFAULT=="1"){
									$("#j_dept").append('<option class=dept value=default>'+obj.DEPTNAME+'</option>');
								}else{
									$("#j_dept").append('<option class=dept value='+obj.DEPTID+'>'+obj.DEPTNAME+'</option>');
								}
							});
						}else{
							$("#j_dept").append('<option class=dept value=default>默认</option>');
						}
					}else{
						$("#j_dept").append('<option class=dept value=default>默认</option>');
					}
					$("#j_dept").trigger("chosen:updated");
			}
		});
	});
});
</script>
</body>
</html>
